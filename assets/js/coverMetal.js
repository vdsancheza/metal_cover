var coverMetal = {};
coverMetal.key = "AIzaSyA6Pp6jiGBaHza02pYq07k_0N-g2PkccuU";



coverMetal.init = function() {
	$("#submit").on("click", function(e){
		e.preventDefault();
		var searchInput = $("#search").val();
		var inputWithCover = searchInput + ' metal cover';
		coverMetal.changeDisplay();
		coverMetal.getVideo(inputWithCover);
		coverMetal.displayVideo();
	});
};


//Functions

coverMetal.getVideo = function(query){
	$.ajax({
		type: "GET",
		url: "https://www.googleapis.com/youtube/v3/search",
		data: {
			v:3,
			q: query,
			part: "snippet",
			type: "video",
			videoEmbeddable: "true",
			maxResults: 1,
			key: coverMetal.key
		},
		success: function(result){
			var theVideoURL = result.items[0].id.videoId;
			coverMetal.displayVideo(theVideoURL);
		}
	});
};


coverMetal.changeDisplay = function(){
	$("body").addClass("showVideo");
};

coverMetal.displayVideo = function(data){
	$(".videoBox").html(
		'<object width="100%" height="500px"><param name="movie" value="https://www.youtube.com/v/'+data+'?version=3"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/'+data+'?version=3" type="application/x-shockwave-flash" width="100%" height="500px" allowscriptaccess="always" allowfullscreen="true"></embed></object>'
	);
};

//Initializr the app with Jquery

$(function(){
	coverMetal.init();
	new Share("#share", {		
		networks: {
			twitter: {
				description: "Just found a great cover song using CoverMe!",
				url: "http://robotprogramador.com/users/@vdsancheza/projects/metal_cover"
			},
			facebook: {
				url:"http://robotprogramador.com/users/@vdsancheza/projects/metal_cover",
				image:"images/rock-type-skeleton-hand-black.svg"
			},
			email: {
				title:"Check out this app to find great cover songs!",
				description:"Here's the link: http://robotprogramador.com/users/@vdsancheza/projects/metal_cover"
			},
			google_plus: {
				enabled:false
			},
			pinterest: {
				enabled:false
			}
		}
	});
});
